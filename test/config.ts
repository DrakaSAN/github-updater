import { describe, it } from 'mocha';
import { isConfig } from '../lib/config';
import { readFile } from 'fs/promises';
import { readdirSync } from 'fs';
import { expect } from 'chai';
import * as path from 'path';

const apps = readdirSync(path.resolve(__dirname, '..', 'config')).map((f) => f.slice(0, -5));
const defaultUserConfig = {
	storage: '~/bin',
	gitlab: { personalToken: '' },
	versions: {}
};

describe('config files', () => {
	apps.forEach((app) => {
		it(app, async () => {
			const file = await readFile(path.resolve(__dirname, '..', 'config', `${app}.json`), { encoding: 'utf-8' });
			const appConfig = JSON.parse(file);
			const fullAppConfig = { ...defaultUserConfig, ...appConfig };

			expect(isConfig(fullAppConfig)).to.be.true;
		});
	});
});
