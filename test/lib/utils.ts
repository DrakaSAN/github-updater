import * as chai from 'chai';
import { describe, it } from 'mocha';
import * as sinon from 'sinon';
import sinonChai from 'sinon-chai';
import chaiAsPromised from 'chai-as-promised';
import * as fs from 'fs';
import { read, write } from '../../lib/utils';

chai.use(sinonChai);
chai.use(chaiAsPromised);

const { expect } = chai;

describe('utils', (): void => {
	describe('read', (): void => {
		afterEach((): void => {
			sinon.restore();
		});

		it('reads and parses the file', async (): Promise<void> => {
			const object = { foo: 'bar' };
			const readFile = sinon.stub(fs.promises, 'readFile').resolves('data');
			const parse = sinon.stub(JSON, 'parse').resolves(object);

			const result = await read('test');

			expect(result).to.equal(object);
			expect(readFile).to.have.been.calledOnceWith('test');
			expect(parse).to.have.been.calledOnceWith('data');
		});

		it('throws on errors', async (): Promise<void> => {
			const error = new Error();

			const readFile = sinon.stub(fs.promises, 'readFile').rejects(error);
			const parse = sinon.stub(JSON, 'parse');

			await expect(read('test')).to.be.rejectedWith(error);
			expect(readFile).to.have.been.calledOnceWith('test');
			expect(parse).to.not.have.been.called;
		});
	});

	describe('write', (): void => {
		afterEach((): void => {
			sinon.restore();
		});

		it('write the file as json', async (): Promise<void> => {
			const object = { foo: 'bar' };

			const writeFile = sinon.stub(fs.promises, 'writeFile');
			const stringify = sinon.stub(JSON, 'stringify').returns('data');

			await write('test', object);

			expect(stringify).to.have.been.calledOnceWith(object);
			expect(writeFile).to.have.been.calledOnceWith('test', 'data');
		});
	});
});
