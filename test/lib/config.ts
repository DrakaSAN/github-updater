import * as chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { describe, it } from 'mocha';
import * as os from 'os';
import * as sinon from 'sinon';
import sinonChai from 'sinon-chai';
import { global, get, purgeGlobalConfigCache, Config } from '../../lib/config';
import * as utils from '../../lib/utils';

chai.use(sinonChai);
chai.use(chaiAsPromised);

const { expect } = chai;

describe('config', (): void => {
	describe('global', (): void => {
		afterEach((): void => {
			purgeGlobalConfigCache();
			sinon.restore();
		});

		it('fetch the global config', async (): Promise<void> => {
			const config = {};
			const read = sinon.stub(utils, 'read').resolves(config);
			expect(await global()).to.equal(config);
			expect(read).to.have.been.calledOnce;
		});

		it('does not read the file on each calls', async (): Promise<void> => {
			const config = {};
			const read = sinon.stub(utils, 'read').resolves(config);
			await global();
			expect(await global()).to.equal(config);
			expect(read).to.have.been.calledOnce;
		});
	});

	describe('get', (): void => {
		const c: Config = {
			binDir: 'binDir',
			alias: 'alias',
			owner: 'owner',
			repo: 'repo',
			source: 'github',
			storage: '/path/to/storage'
		};
		const config = c as unknown as Record<string, unknown>;

		afterEach((): void => {
			purgeGlobalConfigCache();
			sinon.restore();
		});

		it('returns the config', async (): Promise<void> => {
			sinon.stub(utils, 'read')
				.onFirstCall().resolves({})
				.onSecondCall().resolves(config);

			expect(await get('test')).to.deep.equal(config);
		});

		it('fills missing elements from the config with the global config', async (): Promise<void> => {
			sinon.stub(utils, 'read')
				.onFirstCall().resolves({ alias: 'foo' })
				.onSecondCall().resolves({ ...config, alias: undefined });

			expect(await get('test')).to.deep.equal({ ...config, alias: 'foo' });
		});

		it('errors out on invalid config files', async (): Promise<void> => {
			sinon.stub(utils, 'read')
				.onFirstCall().resolves({})
				.onSecondCall().resolves({ ...config, source: undefined });

			await expect(get('test')).to.eventually.be.rejected;
		});

		it('errors out on invalid config files', async (): Promise<void> => {
			sinon.stub(utils, 'read')
				.onFirstCall().resolves({})
				.onSecondCall().resolves({ ...config, source: 219 });

			await expect(get('test')).to.eventually.be.rejected;
		});

		it('replaces ~ by homedir in entries that are file path', async (): Promise<void> => {
			sinon.stub(utils, 'read')
				.onFirstCall().resolves({})
				.onSecondCall().resolves({
					...config,
					alias: '~/alias',
					binDir: '~/binDir'
				});

			expect(await get('test')).to.deep.equal({
				...config,
				alias: `${os.homedir()}/alias`,
				binDir: `${os.homedir()}/binDir`
			});
		});
	});
});