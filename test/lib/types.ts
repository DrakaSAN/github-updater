import * as chai from 'chai';
import { describe, it } from 'mocha';
import { isAsset } from '../../lib/types';

const { expect } = chai;

describe('types', (): void => {
	describe('isAsset', (): void => {
		it('returns false on non assets', (): void => {
			const tests = [
				undefined,
				null,
				0,
				-1,
				1,
				{},
				[],
				{ version: 1 },
				{ version: 'version', possibleConfusion: {} },
				{ version: 'version', appImage: '{}' },
				{ version: 'version', appImage: {} },
				{ version: 'version', appImage: { name: 'name' } },
				{ version: 'version', appImage: { url: 'url' } },
				{ version: 'version', appImage: { name: '', url: 'url' } },
				{ version: 'version', appImage: { name: 'name', url: '' } }
			];

			tests.forEach((test: unknown) => {
				expect(isAsset(test)).to.equal(false, `${JSON.stringify(test)} should not be a valid asset`);
			});
		});

		it('returns true on assets', (): void => {
			const tests = [
				{ version: 'version' },
				{ version: 'version', possibleConfusion: [] },
				{ version: 'version', appImage: { name: 'name', url: 'url' } },
				{ version: 'version', possibleConfusion: [], appImage: { name: 'name', url: 'url' } }
			];

			tests.forEach((test: unknown) => {
				expect(isAsset(test)).to.equal(true, `${JSON.stringify(test)} should be a valid asset`);
			});
		});
	});
});