import { CommonConfig } from '../config';
import { Getter, ConfigChecker } from '../types';
import * as github from './github';
import * as githubTrackedVersion from './github-tracked-version';
import * as gitlab from './gitlab';

// Getter for the version and link to the current appimage
export const getter: Record<string, Getter> = {
	github: github.get,
	'github-tracked-version': githubTrackedVersion.get,
	gitlab: gitlab.get
};

// Helper function to ensure config file validity
export const isConfig: Record<string, ConfigChecker> = {
	github: github.isConfig,
	'github-tracked-version': githubTrackedVersion.isConfig,
	gitlab: gitlab.isConfig
};

export type Config = (
	(CommonConfig & github.Config)
	| (CommonConfig & githubTrackedVersion.Config)
	| (CommonConfig & gitlab.Config)
);
