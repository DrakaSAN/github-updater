import got from 'got';
import type { Types } from '@gitbeaker/node';
import { Asset, Getter } from '../types';
import { global, globalConfigFilePath } from '../config';

export interface Config {
	projectId: string,
	assetSelector?: string
}

export function isConfig(data: unknown): data is Config {
	if (!data && typeof data !== 'object') { return false; }
	const objData = data as Record<string, unknown>;

	return (
		!!objData.projectId && typeof objData.projectId === 'string'
		&& ((!!objData.assetSelector && typeof objData.assetSelector === 'string') || true)
	);
}

let personalToken: string | undefined;

export const get: Getter = async function get(options: unknown): Promise<Asset> {
	const { projectId, assetSelector } = options as Config;
	if (!personalToken) {
		personalToken = (await global()).gitlab.personalToken;
		if (!personalToken) {
			console.error(`No gitlab token configured in ${globalConfigFilePath}`);
			console.error('Please set up a valid Gitlab Personal Access Token in the config file');
			console.error('You can put it in the gitlab.personalToken property');
			console.error('To create one, go to https://gitlab.com/-/profile/personal_access_tokens');
			console.error('The token will need the "read_api" scope');
			throw new Error('No gitlab personal account set');
		}
	}

	const releases = await got.get(
		`https://gitlab.com/api/v4/projects/${projectId}/releases`,
		{ headers: { 'PRIVATE-TOKEN': personalToken } }
	).json();

	const latestRelease = (releases as unknown as Array<Types.ReleaseSchema>)[0];

	if (!latestRelease.assets.links || latestRelease.assets.links.length === 0) {
		throw new Error('Release without any resources');
	}

	const appImages = latestRelease.assets.links.filter((asset) => asset.name.endsWith('.AppImage'));

	if (appImages.length === 0) {
		return {
			version: latestRelease.name
		};
	}

	if (appImages.length > 1 && assetSelector) {
		const selector = new RegExp(assetSelector);
		const appImage = appImages.find((app) => selector.test(app.name));

		if (appImage) {
			return {
				version: latestRelease.name,
				appImage: {
					name: appImage.name,
					url: appImage.url
				}
			};
		}
	}

	return {
		version: latestRelease.name,
		possibleConfusion: appImages.slice(1).map((app) => app.name),
		appImage: {
			name: appImages[0].name,
			url: appImages[0].url
		}
	};
};
