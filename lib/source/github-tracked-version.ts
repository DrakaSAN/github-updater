import got from 'got';
import { Asset, Getter } from '../types';
import { toResponse } from './github';

export interface Config {
	owner: string
	repo: string
	name: string
	url: string
}

export function isConfig(data: unknown): data is Config {
	if (!data && typeof data !== 'object') { return false; }
	const objData = data as Record<string, unknown>;

	return (
		!!objData.owner && typeof objData.owner === 'string'
		&& !!objData.repo && typeof objData.repo === 'string'
		&& !!objData.url && typeof objData.url === 'string'
		&& !!objData.name && typeof objData.name === 'string'
	);
}

export const get: Getter = async function get(options: unknown): Promise<Asset> {
	if (!isConfig(options)) { throw new Error('meh'); }
	const { owner, repo, url, name } = options;
	const response = await got(
		`https://api.github.com/repos/${owner}/${repo}/releases/latest`,
		{
			responseType: 'json',
			headers: {
				Accept: 'application/vnd.github.v3+json'
			}
		}
	);
	const latestRelease = toResponse(response.body);

	return {
		version: latestRelease.name,
		appImage: {
			name: `${name}_${latestRelease.name}`,
			url
		}
	};
};
