import got from 'got';
import { Asset, Getter } from '../types';

interface GitHubResponse {
	name: string,
	assets: Array<GitHubAsset>
}

interface GitHubAsset {
	name: string,
	created_at: string,
	browser_download_url: string
}

export function isGitHubAsset(data: unknown): data is GitHubAsset {
	if (!data || typeof data !== 'object') { return false; }

	const objData = data as Record<string, unknown>;

	return (
		typeof objData.name === 'string'
		&& !!objData.created_at && typeof objData.created_at === 'string'
		&& !!objData.browser_download_url && typeof objData.browser_download_url === 'string'
	);
}

export function toResponse(data: unknown): GitHubResponse {
	if (typeof data !== 'object' || !data) {
		throw new Error();
	}

	const objData = data as Record<string, unknown>;
	objData.name = objData.name || objData.tag_name;

	if (
		typeof objData.name !== 'string'
		|| !objData.assets || !Array.isArray(objData.assets)
		|| !objData.assets.every(isGitHubAsset)
	) {
		console.error(data);

		throw new Error('Invalid github response');
	}

	return {
		name: objData.name,
		assets: objData.assets
	};
}

/**
 * binDir: Path where the AppImage are stored
 * alias: Name of the symbolic link to the latest AppImage
 * owner: Owner of the repository
 * repo: Repository containing the releases
 */
export interface Config {
	owner: string
	repo: string
}

export function isConfig(data: unknown): data is Config {
	if (!data && typeof data !== 'object') { return false; }
	const objData = data as Record<string, unknown>;

	return (
		!!objData.owner && typeof objData.owner === 'string'
		&& !!objData.repo && typeof objData.repo === 'string'
	);
}

export const get: Getter = async function get(options: unknown): Promise<Asset> {
	if (!isConfig(options)) { throw new Error('meh'); }
	const { owner, repo } = options;
	const response = await got(
		`https://api.github.com/repos/${owner}/${repo}/releases/latest`,
		{
			responseType: 'json',
			headers: {
				Accept: 'application/vnd.github.v3+json'
			}
		}
	);
	const latestRelease = toResponse(response.body);

	let possibleConfusion;

	const appImageAssets = latestRelease.assets
		.filter((e: { name: string }) => e.name.endsWith('.AppImage'))
		.sort((a: { created_at: string }, b: { created_at: string }) => {
			if (a.created_at < b.created_at) {return -1;}
			if (a.created_at > b.created_at) {return 1;}
			return 0;
		});

	if (appImageAssets.length === 0) {
		return {
			version: latestRelease.name
		};
	}

	if (appImageAssets.length > 1) {
		possibleConfusion = appImageAssets.map((e: { name: string }) => e.name);
	}

	const asset = appImageAssets[0];

	return {
		version: latestRelease.name,
		possibleConfusion,
		appImage: {
			name: asset.name,
			url: asset.browser_download_url
		}
	};
};
