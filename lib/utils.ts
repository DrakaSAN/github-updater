import * as fs from 'fs';

export type FsError = Error & { code: string };

export async function read(file: string): Promise<Record<string, unknown>> {
	const data = await fs.promises.readFile(file, { encoding: 'utf-8' });
	return JSON.parse(data);
}

export async function write(file: string, content: Record<string, unknown>): Promise<void> {
	return fs.promises.writeFile(file, JSON.stringify(content));
}