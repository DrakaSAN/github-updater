export interface Asset {
	version: string,
	// version of the AppImage
	possibleConfusion?: string[],
	// If there was multiple AppImage in the release, list all of them, only the first is downloaded
	appImage?: {
		name: string,
		// Name of the downloaded AppImage
		url: string
		// Link to download the AppImage
	}
}

export function isAsset(data: unknown): data is Asset {
	if (!data || typeof data !== 'object') { return false; }

	const objData = data as Record<string, unknown>;

	if (!objData.version || typeof objData.version !== 'string') { return false; }
	if (
		objData.possibleConfusion && (
			!Array.isArray(objData.possibleConfusion)
			|| !objData.possibleConfusion.every((el: unknown) => typeof el === 'string')
		)
	) {
		return false;
	}
	if (!objData.appImage) {
		return true;
	}
	if (typeof objData.appImage !== 'object') { return false; }
	const appImage = objData.appImage as Record<string, unknown>;
	return (!!appImage.name && typeof appImage.name === 'string' && !!appImage.url && typeof appImage.url === 'string');
}

export type Getter = (options: unknown) => Promise<Asset>;
export type ConfigChecker = (data: unknown) => boolean;
