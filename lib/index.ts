import * as fs from 'fs';
import * as path from 'path';
import * as stream from 'stream';
import { promisify } from 'util';
import got from 'got';
export * as config from './config';
import { Config, getter } from './source';

export const pipeline = promisify(stream.pipeline);
export const remove = promisify(fs.unlink);

type FsError = Error & { code: string };

async function ensureOutputExist(output: string): Promise<void> {
	try {
		await fs.promises.access(output, fs.constants.W_OK);
	} catch (error) {
		if ((error as FsError).code === 'ENOENT') {
			console.log(`${output} does not exist, it will be created`);
			await fs.promises.mkdir(output);
			return ensureOutputExist(output);
		} else {
			throw error;
		}
	}
}

export function download(url: string, output: string): Promise<void> {
	return pipeline(
		got.stream(url),
		fs.createWriteStream(output)
	);
}

export function flagExecute(file: string): Promise<void> {
	return new Promise((resolve, reject) => {
		fs.chmod(file, '774', (error) => {
			if (error) { reject(error); }
			resolve();
		});
	});
}

interface UpdateResult {
	version: string,
	possibleConfusion?: string[]
}

export async function update(config: Config, currentVersion = ''): Promise<UpdateResult> {
	const { storage, source, binDir, alias, flags } = config;
	if (!getter[source]) { throw new Error(`Unsupported source ${source}`); }

	const { version, appImage, possibleConfusion } = await getter[source](config);

	if (version === currentVersion) { return { version }; }
	if (!appImage) { throw new Error(`There is a new version (${version}), but no AppImage associated`); }

	const binaryPath = path.resolve(storage, binDir, appImage.name);
	const aliasPath = flags ? path.resolve(storage, `${alias}-no-flags`) : path.resolve(storage, alias);

	// console.log(`binaryPath: ${binaryPath}`);
	// console.log(`aliasPath: ${aliasPath}`);

	await ensureOutputExist(path.dirname(aliasPath));
	await ensureOutputExist(path.dirname(binaryPath));
	await download(appImage.url, binaryPath);
	await flagExecute(binaryPath);

	if (flags) {
		const execWithFlag = path.resolve(storage, alias);
		const content = `#! /bin/sh\n${alias}-no-flags $@ ${flags}\n`;

		// console.log(`execWithFlag: ${execWithFlag}`);

		await ensureOutputExist(path.dirname(execWithFlag));
		await fs.promises.writeFile(execWithFlag, content, { encoding: 'utf-8' });
		await flagExecute(execWithFlag);
	}

	try {
		await fs.promises.symlink(binaryPath, aliasPath);
	} catch (error) {
		if ((error as FsError).code === 'EEXIST') {
			await remove(aliasPath);
			await fs.promises.symlink(binaryPath, aliasPath);
		} else {
			throw error;
		}
	}

	return { version, possibleConfusion };
}
