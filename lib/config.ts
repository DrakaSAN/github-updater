import { homedir } from 'os';
import * as path from 'path';
import { readdir } from 'fs/promises';
import { FsError, read, write } from './utils';
import * as source from './source';

export const globalConfigFilePath = path.resolve(homedir(), '.config', 'd219-update-appimage.json');
export const configFolder = path.resolve(__dirname, '..', '..', 'config');

export type Versions = { [app: string]: string; };

export type UserConfig = {
	storage: string;
	gitlab: { personalToken: string; };
	versions: Versions;
}

export type CommonConfig = {
	binDir: string;
	alias: string;
	source: string;
	storage: string;
	flags?: string;
}

export type Config = source.Config;

let _globalConfig: UserConfig | undefined;

export function purgeGlobalConfigCache(): void {
	_globalConfig = undefined;
}

/**
 * Get the global configuration.
 * That configuration is cached after the first read.
 * @returns Global config
 */
export async function global(): Promise<UserConfig> {
	if (!_globalConfig) {
		try {
			_globalConfig = await read(globalConfigFilePath) as UserConfig;
		} catch (error) {
			if ((error as FsError).code === 'ENOENT') {
				console.error(`Config file not found at ${globalConfigFilePath}`);
				console.error('Will try to create default one');
				_globalConfig = {
					storage: `${homedir()}/bin`,
					gitlab: { personalToken: '' },
					versions: {}
				};
				await write(globalConfigFilePath, _globalConfig);
				return _globalConfig;
			}
			throw error;
		}
	}
	return _globalConfig;
}

export async function updateVersions(versions: Versions): Promise<void> {
	if (!_globalConfig) { throw new Error('Cannot update version on unloaded config'); }
	_globalConfig.versions = versions;
	await write(globalConfigFilePath, _globalConfig);
}

export function isUserConfig(data: unknown): data is UserConfig {
	if (!data || typeof data !== 'object') { return false; }

	const objData = data as UserConfig;

	return (
		!!objData.storage && typeof objData.storage === 'string'
		&& !!objData.gitlab && typeof objData.gitlab === 'object'
		&& !!objData.gitlab.personalToken && typeof objData.gitlab.personalToken === 'string'
		&& !!objData.versions && typeof objData.versions === 'object'
		&& Object.keys(objData.versions).every((app) => typeof objData.versions[app] === 'string')
	);
}

export function isCommonConfig(data: unknown): data is CommonConfig {
	if (!data || typeof data !== 'object') { return false; }

	const objData = data as CommonConfig;

	return (
		!!objData.storage && typeof objData.storage === 'string'
		&& !!objData.binDir && typeof objData.binDir === 'string'
		&& !!objData.alias && typeof objData.alias === 'string'
		&& !!objData.source && typeof objData.source === 'string'
		&& (
			!objData.flags
			|| (!!objData.flags && typeof objData.flags === 'string')
		)
	);
}

export function isConfig(data: unknown): data is source.Config {
	return isCommonConfig(data) && source.isConfig[data.source](data);
}

/**
 * Get the config for a specific app
 *
 * @param app - config to get
 * @returns app's config
 */
export async function get(app: string): Promise<Config> {
	let specific;
	try {
		specific = await read(path.resolve(configFolder, `${app}.json`));
	} catch (error) {
		if ((error as FsError).code === 'ENOENT') {
			console.error(`No config found for ${app}`);
		}
		throw error;
	}
	const globalConfig = await global();
	const config = { ...globalConfig, ...specific };

	if (!isConfig(config)) { throw new Error(`Invalid configuration for ${app}`); }
	const pathKeys = ['storage', 'binDir', 'alias'];
	type PathKey = 'storage' | 'binDir' | 'alias';
	pathKeys.forEach((key: string) => {
		config[key as PathKey] = (config[key as PathKey]).replace('~', homedir()).replace('$HOME', homedir());
	});

	return config as Config;
}

export async function list(): Promise<string[]> {
	const configFiles = await readdir(configFolder);
	return configFiles
		// remove '.json'
		.map((f) => f.slice(0, -5));
}
