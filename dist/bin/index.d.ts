#!/usr/bin/env node
export declare function updatePackages(apps: string[]): Promise<void>;
export declare function run(args: string[]): Promise<void>;
