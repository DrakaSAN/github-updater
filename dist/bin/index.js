#!/usr/bin/env node
"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.run = exports.updatePackages = void 0;
var yargs_1 = __importDefault(require("yargs"));
var helpers_1 = require("yargs/helpers");
var lib_1 = require("../lib");
function updatePackages(apps) {
    return __awaiter(this, void 0, void 0, function () {
        var globalConfig, versions, results, errors;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, lib_1.config.global()];
                case 1:
                    globalConfig = _a.sent();
                    versions = globalConfig.versions;
                    console.log("Update: \n- ".concat(apps.join('\n- ')));
                    return [4, Promise.all(apps.map(function (app) { return __awaiter(_this, void 0, void 0, function () {
                            var args, error_1, result, error_2;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        _a.trys.push([0, 2, , 3]);
                                        return [4, lib_1.config.get(app)];
                                    case 1:
                                        args = _a.sent();
                                        return [3, 3];
                                    case 2:
                                        error_1 = _a.sent();
                                        return [2, { app: app, error: error_1 }];
                                    case 3:
                                        _a.trys.push([3, 5, , 6]);
                                        return [4, (0, lib_1.update)(__assign({}, args), "".concat(versions[app]) || '')];
                                    case 4:
                                        result = _a.sent();
                                        return [3, 6];
                                    case 5:
                                        error_2 = _a.sent();
                                        return [2, { app: app, error: error_2 }];
                                    case 6: return [2, { app: app, result: result }];
                                }
                            });
                        }); }))];
                case 2:
                    results = _a.sent();
                    results.forEach(function (result) {
                        if (!result.result) {
                            return;
                        }
                        var app = result.app;
                        var version = result.result.version;
                        if (versions[app] !== version) {
                            console.log("".concat(app, ": ").concat(versions[app], " => ").concat(version));
                            versions[app] = version;
                        }
                        else {
                            console.log("".concat(app, " already up to date (").concat(version, ")"));
                        }
                    });
                    return [4, lib_1.config.updateVersions(versions)];
                case 3:
                    _a.sent();
                    errors = results.filter(function (r) { return !!r.error; });
                    if (errors.length !== 0) {
                        console.error("Errors when updating ".concat(errors.map(function (r) { return r.app; })));
                        errors.forEach(function (result) {
                            var app = result.app, error = result.error;
                            console.error(app);
                            console.error(error);
                            console.error();
                        });
                        process.exit(1);
                    }
                    process.exit(0);
                    return [2];
            }
        });
    });
}
exports.updatePackages = updatePackages;
function run(args) {
    return __awaiter(this, void 0, void 0, function () {
        var options, globalConfig, globalConfig, configs, versions_1, output, globalConfig, apps, error_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    options = yargs_1.default
                        .option('install', {
                        alias: ['i', 'update', 'u'],
                        type: 'string',
                        array: true,
                        description: 'Install and/or update packages'
                    })
                        .option('list', {
                        alias: 'l',
                        type: 'boolean',
                        description: 'List the currently supported packages'
                    })
                        .option('installed', {
                        type: 'boolean',
                        description: 'Only list installed packages',
                        implies: 'list'
                    })
                        .parse((0, helpers_1.hideBin)(args));
                    if (!options.install) {
                        options.install = [];
                    }
                    options.install = options.install.concat(options._);
                    if (!(options.install.length === 0)) return [3, 2];
                    return [4, lib_1.config.global()];
                case 1:
                    globalConfig = _a.sent();
                    options.install = Object.keys(globalConfig.versions);
                    _a.label = 2;
                case 2:
                    if (!(options.list && !options.installed)) return [3, 5];
                    return [4, lib_1.config.global()];
                case 3:
                    globalConfig = _a.sent();
                    return [4, lib_1.config.list()];
                case 4:
                    configs = _a.sent();
                    versions_1 = globalConfig.versions;
                    output = configs
                        .map(function (c) {
                        if (versions_1[c]) {
                            return "".concat(c, " [installed] (").concat(versions_1[c], ")");
                        }
                        return c;
                    })
                        .filter(function (c) { return !!c; })
                        .join('\n');
                    console.log(output);
                    process.exit(0);
                    return [3, 7];
                case 5:
                    if (!(options.list && options.installed)) return [3, 7];
                    return [4, lib_1.config.global()];
                case 6:
                    globalConfig = _a.sent();
                    console.log(Object.keys(globalConfig.versions).join('\n'));
                    process.exit(0);
                    _a.label = 7;
                case 7:
                    apps = Array.from(new Set(options.install));
                    _a.label = 8;
                case 8:
                    _a.trys.push([8, 10, , 11]);
                    return [4, updatePackages(apps)];
                case 9:
                    _a.sent();
                    return [3, 11];
                case 10:
                    error_3 = _a.sent();
                    console.error(error_3);
                    process.exit(1);
                    return [3, 11];
                case 11: return [2];
            }
        });
    });
}
exports.run = run;
if (require.main === module) {
    run(process.argv);
}
