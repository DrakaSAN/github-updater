"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isAsset = void 0;
function isAsset(data) {
    if (!data || typeof data !== 'object') {
        return false;
    }
    var objData = data;
    if (!objData.version || typeof objData.version !== 'string') {
        return false;
    }
    if (objData.possibleConfusion && (!Array.isArray(objData.possibleConfusion)
        || !objData.possibleConfusion.every(function (el) { return typeof el === 'string'; }))) {
        return false;
    }
    if (!objData.appImage) {
        return true;
    }
    if (typeof objData.appImage !== 'object') {
        return false;
    }
    var appImage = objData.appImage;
    return (!!appImage.name && typeof appImage.name === 'string' && !!appImage.url && typeof appImage.url === 'string');
}
exports.isAsset = isAsset;
