"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.update = exports.flagExecute = exports.download = exports.remove = exports.pipeline = exports.config = void 0;
var fs = __importStar(require("fs"));
var path = __importStar(require("path"));
var stream = __importStar(require("stream"));
var util_1 = require("util");
var got_1 = __importDefault(require("got"));
exports.config = __importStar(require("./config"));
var source_1 = require("./source");
exports.pipeline = (0, util_1.promisify)(stream.pipeline);
exports.remove = (0, util_1.promisify)(fs.unlink);
function ensureOutputExist(output) {
    return __awaiter(this, void 0, void 0, function () {
        var error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 6]);
                    return [4, fs.promises.access(output, fs.constants.W_OK)];
                case 1:
                    _a.sent();
                    return [3, 6];
                case 2:
                    error_1 = _a.sent();
                    if (!(error_1.code === 'ENOENT')) return [3, 4];
                    console.log("".concat(output, " does not exist, it will be created"));
                    return [4, fs.promises.mkdir(output)];
                case 3:
                    _a.sent();
                    return [2, ensureOutputExist(output)];
                case 4: throw error_1;
                case 5: return [3, 6];
                case 6: return [2];
            }
        });
    });
}
function download(url, output) {
    return (0, exports.pipeline)(got_1.default.stream(url), fs.createWriteStream(output));
}
exports.download = download;
function flagExecute(file) {
    return new Promise(function (resolve, reject) {
        fs.chmod(file, '774', function (error) {
            if (error) {
                reject(error);
            }
            resolve();
        });
    });
}
exports.flagExecute = flagExecute;
function update(config, currentVersion) {
    if (currentVersion === void 0) { currentVersion = ''; }
    return __awaiter(this, void 0, void 0, function () {
        var storage, source, binDir, alias, flags, _a, version, appImage, possibleConfusion, binaryPath, aliasPath, execWithFlag, content, error_2;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    storage = config.storage, source = config.source, binDir = config.binDir, alias = config.alias, flags = config.flags;
                    if (!source_1.getter[source]) {
                        throw new Error("Unsupported source ".concat(source));
                    }
                    return [4, source_1.getter[source](config)];
                case 1:
                    _a = _b.sent(), version = _a.version, appImage = _a.appImage, possibleConfusion = _a.possibleConfusion;
                    if (version === currentVersion) {
                        return [2, { version: version }];
                    }
                    if (!appImage) {
                        throw new Error("There is a new version (".concat(version, "), but no AppImage associated"));
                    }
                    binaryPath = path.resolve(storage, binDir, appImage.name);
                    aliasPath = flags ? path.resolve(storage, "".concat(alias, "-no-flags")) : path.resolve(storage, alias);
                    return [4, ensureOutputExist(path.dirname(aliasPath))];
                case 2:
                    _b.sent();
                    return [4, ensureOutputExist(path.dirname(binaryPath))];
                case 3:
                    _b.sent();
                    return [4, download(appImage.url, binaryPath)];
                case 4:
                    _b.sent();
                    return [4, flagExecute(binaryPath)];
                case 5:
                    _b.sent();
                    if (!flags) return [3, 9];
                    execWithFlag = path.resolve(storage, alias);
                    content = "#! /bin/sh\n".concat(alias, "-no-flags $@ ").concat(flags, "\n");
                    return [4, ensureOutputExist(path.dirname(execWithFlag))];
                case 6:
                    _b.sent();
                    return [4, fs.promises.writeFile(execWithFlag, content, { encoding: 'utf-8' })];
                case 7:
                    _b.sent();
                    return [4, flagExecute(execWithFlag)];
                case 8:
                    _b.sent();
                    _b.label = 9;
                case 9:
                    _b.trys.push([9, 11, , 16]);
                    return [4, fs.promises.symlink(binaryPath, aliasPath)];
                case 10:
                    _b.sent();
                    return [3, 16];
                case 11:
                    error_2 = _b.sent();
                    if (!(error_2.code === 'EEXIST')) return [3, 14];
                    return [4, (0, exports.remove)(aliasPath)];
                case 12:
                    _b.sent();
                    return [4, fs.promises.symlink(binaryPath, aliasPath)];
                case 13:
                    _b.sent();
                    return [3, 15];
                case 14: throw error_2;
                case 15: return [3, 16];
                case 16: return [2, { version: version, possibleConfusion: possibleConfusion }];
            }
        });
    });
}
exports.update = update;
