import * as source from './source';
export declare const globalConfigFilePath: string;
export declare const configFolder: string;
export declare type Versions = {
    [app: string]: string;
};
export declare type UserConfig = {
    storage: string;
    gitlab: {
        personalToken: string;
    };
    versions: Versions;
};
export declare type CommonConfig = {
    binDir: string;
    alias: string;
    source: string;
    storage: string;
    flags?: string;
};
export declare type Config = source.Config;
export declare function purgeGlobalConfigCache(): void;
export declare function global(): Promise<UserConfig>;
export declare function updateVersions(versions: Versions): Promise<void>;
export declare function isUserConfig(data: unknown): data is UserConfig;
export declare function isCommonConfig(data: unknown): data is CommonConfig;
export declare function isConfig(data: unknown): data is source.Config;
export declare function get(app: string): Promise<Config>;
export declare function list(): Promise<string[]>;
