import { Getter } from '../types';
export interface Config {
    owner: string;
    repo: string;
    name: string;
    url: string;
}
export declare function isConfig(data: unknown): data is Config;
export declare const get: Getter;
