"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.get = exports.isConfig = void 0;
var got_1 = __importDefault(require("got"));
var config_1 = require("../config");
function isConfig(data) {
    if (!data && typeof data !== 'object') {
        return false;
    }
    var objData = data;
    return (!!objData.projectId && typeof objData.projectId === 'string'
        && ((!!objData.assetSelector && typeof objData.assetSelector === 'string') || true));
}
exports.isConfig = isConfig;
var personalToken;
var get = function get(options) {
    return __awaiter(this, void 0, void 0, function () {
        var _a, projectId, assetSelector, releases, latestRelease, appImages, selector_1, appImage;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _a = options, projectId = _a.projectId, assetSelector = _a.assetSelector;
                    if (!!personalToken) return [3, 2];
                    return [4, (0, config_1.global)()];
                case 1:
                    personalToken = (_b.sent()).gitlab.personalToken;
                    if (!personalToken) {
                        console.error("No gitlab token configured in ".concat(config_1.globalConfigFilePath));
                        console.error('Please set up a valid Gitlab Personal Access Token in the config file');
                        console.error('You can put it in the gitlab.personalToken property');
                        console.error('To create one, go to https://gitlab.com/-/profile/personal_access_tokens');
                        console.error('The token will need the "read_api" scope');
                        throw new Error('No gitlab personal account set');
                    }
                    _b.label = 2;
                case 2: return [4, got_1.default.get("https://gitlab.com/api/v4/projects/".concat(projectId, "/releases"), { headers: { 'PRIVATE-TOKEN': personalToken } }).json()];
                case 3:
                    releases = _b.sent();
                    latestRelease = releases[0];
                    if (!latestRelease.assets.links || latestRelease.assets.links.length === 0) {
                        throw new Error('Release without any resources');
                    }
                    appImages = latestRelease.assets.links.filter(function (asset) { return asset.name.endsWith('.AppImage'); });
                    if (appImages.length === 0) {
                        return [2, {
                                version: latestRelease.name
                            }];
                    }
                    if (appImages.length > 1 && assetSelector) {
                        selector_1 = new RegExp(assetSelector);
                        appImage = appImages.find(function (app) { return selector_1.test(app.name); });
                        if (appImage) {
                            return [2, {
                                    version: latestRelease.name,
                                    appImage: {
                                        name: appImage.name,
                                        url: appImage.url
                                    }
                                }];
                        }
                    }
                    return [2, {
                            version: latestRelease.name,
                            possibleConfusion: appImages.slice(1).map(function (app) { return app.name; }),
                            appImage: {
                                name: appImages[0].name,
                                url: appImages[0].url
                            }
                        }];
            }
        });
    });
};
exports.get = get;
