"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.get = exports.isConfig = exports.toResponse = exports.isGitHubAsset = void 0;
var got_1 = __importDefault(require("got"));
function isGitHubAsset(data) {
    if (!data || typeof data !== 'object') {
        return false;
    }
    var objData = data;
    return (typeof objData.name === 'string'
        && !!objData.created_at && typeof objData.created_at === 'string'
        && !!objData.browser_download_url && typeof objData.browser_download_url === 'string');
}
exports.isGitHubAsset = isGitHubAsset;
function toResponse(data) {
    if (typeof data !== 'object' || !data) {
        throw new Error();
    }
    var objData = data;
    objData.name = objData.name || objData.tag_name;
    if (typeof objData.name !== 'string'
        || !objData.assets || !Array.isArray(objData.assets)
        || !objData.assets.every(isGitHubAsset)) {
        console.error(data);
        throw new Error('Invalid github response');
    }
    return {
        name: objData.name,
        assets: objData.assets
    };
}
exports.toResponse = toResponse;
function isConfig(data) {
    if (!data && typeof data !== 'object') {
        return false;
    }
    var objData = data;
    return (!!objData.owner && typeof objData.owner === 'string'
        && !!objData.repo && typeof objData.repo === 'string');
}
exports.isConfig = isConfig;
var get = function get(options) {
    return __awaiter(this, void 0, void 0, function () {
        var owner, repo, response, latestRelease, possibleConfusion, appImageAssets, asset;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!isConfig(options)) {
                        throw new Error('meh');
                    }
                    owner = options.owner, repo = options.repo;
                    return [4, (0, got_1.default)("https://api.github.com/repos/".concat(owner, "/").concat(repo, "/releases/latest"), {
                            responseType: 'json',
                            headers: {
                                Accept: 'application/vnd.github.v3+json'
                            }
                        })];
                case 1:
                    response = _a.sent();
                    latestRelease = toResponse(response.body);
                    appImageAssets = latestRelease.assets
                        .filter(function (e) { return e.name.endsWith('.AppImage'); })
                        .sort(function (a, b) {
                        if (a.created_at < b.created_at) {
                            return -1;
                        }
                        if (a.created_at > b.created_at) {
                            return 1;
                        }
                        return 0;
                    });
                    if (appImageAssets.length === 0) {
                        return [2, {
                                version: latestRelease.name
                            }];
                    }
                    if (appImageAssets.length > 1) {
                        possibleConfusion = appImageAssets.map(function (e) { return e.name; });
                    }
                    asset = appImageAssets[0];
                    return [2, {
                            version: latestRelease.name,
                            possibleConfusion: possibleConfusion,
                            appImage: {
                                name: asset.name,
                                url: asset.browser_download_url
                            }
                        }];
            }
        });
    });
};
exports.get = get;
