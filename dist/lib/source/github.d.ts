import { Getter } from '../types';
interface GitHubResponse {
    name: string;
    assets: Array<GitHubAsset>;
}
interface GitHubAsset {
    name: string;
    created_at: string;
    browser_download_url: string;
}
export declare function isGitHubAsset(data: unknown): data is GitHubAsset;
export declare function toResponse(data: unknown): GitHubResponse;
export interface Config {
    owner: string;
    repo: string;
}
export declare function isConfig(data: unknown): data is Config;
export declare const get: Getter;
export {};
