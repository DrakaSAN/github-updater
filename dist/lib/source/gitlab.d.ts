import { Getter } from '../types';
export interface Config {
    projectId: string;
    assetSelector?: string;
}
export declare function isConfig(data: unknown): data is Config;
export declare const get: Getter;
