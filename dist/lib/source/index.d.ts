import { CommonConfig } from '../config';
import { Getter, ConfigChecker } from '../types';
import * as github from './github';
import * as githubTrackedVersion from './github-tracked-version';
import * as gitlab from './gitlab';
export declare const getter: Record<string, Getter>;
export declare const isConfig: Record<string, ConfigChecker>;
export declare type Config = ((CommonConfig & github.Config) | (CommonConfig & githubTrackedVersion.Config) | (CommonConfig & gitlab.Config));
