export interface Asset {
    version: string;
    possibleConfusion?: string[];
    appImage?: {
        name: string;
        url: string;
    };
}
export declare function isAsset(data: unknown): data is Asset;
export declare type Getter = (options: unknown) => Promise<Asset>;
export declare type ConfigChecker = (data: unknown) => boolean;
