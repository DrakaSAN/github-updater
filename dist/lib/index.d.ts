/// <reference types="node" />
import * as fs from 'fs';
import * as stream from 'stream';
export * as config from './config';
import { Config } from './source';
export declare const pipeline: typeof stream.pipeline.__promisify__;
export declare const remove: typeof fs.unlink.__promisify__;
export declare function download(url: string, output: string): Promise<void>;
export declare function flagExecute(file: string): Promise<void>;
interface UpdateResult {
    version: string;
    possibleConfusion?: string[];
}
export declare function update(config: Config, currentVersion?: string): Promise<UpdateResult>;
