export declare type FsError = Error & {
    code: string;
};
export declare function read(file: string): Promise<Record<string, unknown>>;
export declare function write(file: string, content: Record<string, unknown>): Promise<void>;
