"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.list = exports.get = exports.isConfig = exports.isCommonConfig = exports.isUserConfig = exports.updateVersions = exports.global = exports.purgeGlobalConfigCache = exports.configFolder = exports.globalConfigFilePath = void 0;
var os_1 = require("os");
var path = __importStar(require("path"));
var promises_1 = require("fs/promises");
var utils_1 = require("./utils");
var source = __importStar(require("./source"));
exports.globalConfigFilePath = path.resolve((0, os_1.homedir)(), '.config', 'd219-update-appimage.json');
exports.configFolder = path.resolve(__dirname, '..', '..', 'config');
var _globalConfig;
function purgeGlobalConfigCache() {
    _globalConfig = undefined;
}
exports.purgeGlobalConfigCache = purgeGlobalConfigCache;
function global() {
    return __awaiter(this, void 0, void 0, function () {
        var error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!!_globalConfig) return [3, 6];
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 6]);
                    return [4, (0, utils_1.read)(exports.globalConfigFilePath)];
                case 2:
                    _globalConfig = (_a.sent());
                    return [3, 6];
                case 3:
                    error_1 = _a.sent();
                    if (!(error_1.code === 'ENOENT')) return [3, 5];
                    console.error("Config file not found at ".concat(exports.globalConfigFilePath));
                    console.error('Will try to create default one');
                    _globalConfig = {
                        storage: "".concat((0, os_1.homedir)(), "/bin"),
                        gitlab: { personalToken: '' },
                        versions: {}
                    };
                    return [4, (0, utils_1.write)(exports.globalConfigFilePath, _globalConfig)];
                case 4:
                    _a.sent();
                    return [2, _globalConfig];
                case 5: throw error_1;
                case 6: return [2, _globalConfig];
            }
        });
    });
}
exports.global = global;
function updateVersions(versions) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!_globalConfig) {
                        throw new Error('Cannot update version on unloaded config');
                    }
                    _globalConfig.versions = versions;
                    return [4, (0, utils_1.write)(exports.globalConfigFilePath, _globalConfig)];
                case 1:
                    _a.sent();
                    return [2];
            }
        });
    });
}
exports.updateVersions = updateVersions;
function isUserConfig(data) {
    if (!data || typeof data !== 'object') {
        return false;
    }
    var objData = data;
    return (!!objData.storage && typeof objData.storage === 'string'
        && !!objData.gitlab && typeof objData.gitlab === 'object'
        && !!objData.gitlab.personalToken && typeof objData.gitlab.personalToken === 'string'
        && !!objData.versions && typeof objData.versions === 'object'
        && Object.keys(objData.versions).every(function (app) { return typeof objData.versions[app] === 'string'; }));
}
exports.isUserConfig = isUserConfig;
function isCommonConfig(data) {
    if (!data || typeof data !== 'object') {
        return false;
    }
    var objData = data;
    return (!!objData.storage && typeof objData.storage === 'string'
        && !!objData.binDir && typeof objData.binDir === 'string'
        && !!objData.alias && typeof objData.alias === 'string'
        && !!objData.source && typeof objData.source === 'string'
        && (!objData.flags
            || (!!objData.flags && typeof objData.flags === 'string')));
}
exports.isCommonConfig = isCommonConfig;
function isConfig(data) {
    return isCommonConfig(data) && source.isConfig[data.source](data);
}
exports.isConfig = isConfig;
function get(app) {
    return __awaiter(this, void 0, void 0, function () {
        var specific, error_2, globalConfig, config, pathKeys;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4, (0, utils_1.read)(path.resolve(exports.configFolder, "".concat(app, ".json")))];
                case 1:
                    specific = _a.sent();
                    return [3, 3];
                case 2:
                    error_2 = _a.sent();
                    if (error_2.code === 'ENOENT') {
                        console.error("No config found for ".concat(app));
                    }
                    throw error_2;
                case 3: return [4, global()];
                case 4:
                    globalConfig = _a.sent();
                    config = __assign(__assign({}, globalConfig), specific);
                    if (!isConfig(config)) {
                        throw new Error("Invalid configuration for ".concat(app));
                    }
                    pathKeys = ['storage', 'binDir', 'alias'];
                    pathKeys.forEach(function (key) {
                        config[key] = (config[key]).replace('~', (0, os_1.homedir)()).replace('$HOME', (0, os_1.homedir)());
                    });
                    return [2, config];
            }
        });
    });
}
exports.get = get;
function list() {
    return __awaiter(this, void 0, void 0, function () {
        var configFiles;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, (0, promises_1.readdir)(exports.configFolder)];
                case 1:
                    configFiles = _a.sent();
                    return [2, configFiles
                            .map(function (f) { return f.slice(0, -5); })];
            }
        });
    });
}
exports.list = list;
