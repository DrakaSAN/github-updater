# Appimage Updater

[![pipeline status](https://gitlab.com/DrakaSAN/github-updater/badges/master/pipeline.svg)](https://gitlab.com/DrakaSAN/github-updater/-/commits/master)  [![coverage report](https://gitlab.com/DrakaSAN/github-updater/badges/master/coverage.svg)](https://gitlab.com/DrakaSAN/github-updater/-/commits/master)

Simple CLI app to manage apps that are not in a repository and are supposed to be downloaded from things like github releases.

## Install

```sh
npm install -g @d219/update-appimage
```

## Usage

### Update all installed apps

```sh
update-appimage
```

This will auto detect which apps were installed by this app, check if a new version was released, and download it.

### List apps

```sh
update-appimage --list
```

This will print a list of all supported apps, if the app is installed, it will add `[installed]` and the current version of the app behind it.

Example:

```sh
$ update-appimage --list
chiaki [installed] (v1.3.0)
cozy-drive [installed] (v1.34.0)
etcher
keepassxc [installed] (Release 2.6.4)
pendulums [installed] (v1.1.0)
vscodium [installed] (1.55.0)
```

You can also ask for only the installed apps:

```sh
$ update-appimage --list --installed
cozy-drive
keepassxc
vscodium
chiaki
pendulums
```

### Install a new app

```sh
update-appimage --install app
```

If no other flag is set, `--install` is presupposed, so this is also valid:

```sh
update-appimage app
```

## Support

The installation will put the app in the default npm global path, if you need to find it, you should be able to do so by looking at the `cwd` property when executing `npm config list`. You should then find it under `@d219/update-appimage`.

If you need to modify something like where the binaries and softlink are stored, you should edit the `config/global.json` file and change the `storage` property.

If need be, you can also find the `version.json` file there, which tracks which app is installed and at which version it is.

This app never delete anything, when updating, it download the new appimage, but keeps all previously downloaded versions in their respectives `binDir` (usually named after the appimage, but capitalized). If something breaks, you can delete the alias and recreate the softlink to an older version:

```sh
rm ~/bin/app
ln -s bin/App/app-older.AppImage bin/app
```

## Contribute

### Support a new app

Simply add a new file named after the new app in the `config` folder. Those files are JSON, and are formatted depending on the `source` property. The name of the file will determine the name it will have in the installation/update command.

#### Github

`github` is used to fetch an app in the github releases, `chiaki` for example.

```json
{
    "source": "github",
    "alias": "chiaki",      // How the command will be named
    "binDir": "Chiaki",     // In which folder will the binaries be stored
    "owner": "thestr4ng3r", // Owner of the github repo
    "repo": "chiaki"        // Name of the github repo
}
```

### Github tracked version

Used for apps where the current version can be fetched from github, but the official source for the app image is somewhere else, `cozy-drive` for example.

```json
{
    "source": "github-tracked-version",
    "alias": "cozy",                                                // How the command will be named
    "binDir": "Cozy",                                               // In which folder will the appimages be stored
    "owner": "cozy",                                                // Owner of the github repo
    "repo": "cozy-drive",                                           // Name of the github repo
    "name": "CozyDrive",                                            // How to name the appimage once downloaded
    "url": "https://nuts.cozycloud.cc/download/channel/stable/64"   // Where to download the latest appimage
}
```

### Flags

If for some reason, an app needs some flags to be set to works (for example, electron-based apps on a Linux core that doesn't properly support sandbox), a `flags` property can be set in the config file.
This will generate both a `app-no-flags` with the direct link to the app, and a executable script that will start it with the given flags.

For example:

```json
{
    "binDir": "VSCodium",
    "alias": "codium",
    "source": "github",
    "owner": "VSCodium",
    "repo": "vscodium",
    "flags": "--no-sandbox"
}
```

will generate the `codium-no-flags` executable that works as in other apps, and `codium` that will start it with the `--no-sandbox` flag.
