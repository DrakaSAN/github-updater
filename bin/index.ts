#!/usr/bin/env node

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import { update, config } from '../lib';

export async function updatePackages(apps: string[]): Promise<void> {
	// Get all current installed versions
	const globalConfig = await config.global();
	const versions = globalConfig.versions;

	console.log(`Update: \n- ${apps.join('\n- ')}`);

	// For each app to update
	const results = await Promise.all(apps.map(async (app) => {
		let args;

		// Get the configuration to fetch that app
		try {
			args = await config.get(app);
		} catch (error) {
			return { app, error };
		}

		let result;

		// Update the app
		try {
			result = await update({ ...args }, `${versions[app]}` || '');
		} catch (error) {
			return { app, error };
		}

		return { app, result };
	}));

	// Print updated packages
	results.forEach((result) => {
		if (!result.result) { return; }
		const { app } = result;
		const { version } = result.result;

		if (versions[app] !== version) {
			console.log(`${app}: ${versions[app]} => ${version}`);
			versions[app] = version;
		} else {
			console.log(`${app} already up to date (${version})`);
		}
	});

	// Save the new version file
	await config.updateVersions(versions);


	// Print errors
	const errors = results.filter((r) => !!r.error);

	if (errors.length !== 0) {
		console.error(`Errors when updating ${errors.map((r) => r.app)}`);
		errors.forEach((result) => {
			const { app, error } = result;

			console.error(app);
			console.error(error);
			console.error();
		});

		process.exit(1);
	}

	process.exit(0);
}

export async function run(args: string[]): Promise<void> {
	const options = yargs
		.option('install', {
			alias: ['i', 'update', 'u'],
			type: 'string',
			array: true,
			description: 'Install and/or update packages'
		})
		.option('list', {
			alias: 'l',
			type: 'boolean',
			description: 'List the currently supported packages'
		})
		.option('installed', {
			type: 'boolean',
			description: 'Only list installed packages',
			implies: 'list'
		})
		.parse(hideBin(args));

	// --install is the default command, so coerce install and _
	if (!options.install) { options.install = []; }
	options.install = options.install.concat(options._ as string[]);

	// If install is empty, assume it is all installed apps
	if (options.install.length === 0) {
		const globalConfig = await config.global();
		options.install = Object.keys(globalConfig.versions);
	}

	if (options.list && !options.installed) {
		// List all packages
		const globalConfig = await config.global();
		const configs = await config.list();
		const versions = globalConfig.versions;

		const output = configs
			.map((c) => {
				if (versions[c]) {
					return `${c} [installed] (${versions[c]})`;
				}
				return c;
			})
			.filter((c) => !!c)
			.join('\n');

		console.log(output);
		process.exit(0);
	} else if (options.list && options.installed) {
		const globalConfig = await config.global();
		// List installed packages only
		console.log(Object.keys(globalConfig.versions).join('\n'));
		process.exit(0);
	}

	const apps = Array.from(new Set(options.install));

	try {
		await updatePackages(apps);
	} catch(error) {
		console.error(error);
		process.exit(1);
	}
}

if (require.main === module) {
	run(process.argv);
}
